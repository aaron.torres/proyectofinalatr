package com.softtek.academy.service;

import java.util.List;

import com.softtek.academy.entities.Momentum;

public interface MomentumService {
	

	public abstract Double obtainMonthHours(String name, String mes);

	public abstract List<Momentum> getMomentumbyDate(String name, String day, String month, String year, String enddday,
			String endmonth, String endyear); 

	public abstract List<Momentum> getMomentumByMonth(String month); 

	public abstract List<Momentum> getMomentumsbyDate(String day, String month, String year, String endday,
			String endmonth, String endyear); 

	public abstract List<Momentum> listAllMomentums();
}
