package com.softtek.academy.service;

import java.io.*;
import java.sql.*;
import java.util.*;

import javax.annotation.PostConstruct;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.softtek.academy.entities.Momentum;
import com.softtek.academy.repositories.MomentumJpaRepository;

@Service("excelServiceImpl")
public class ExcelServiceImpl implements ExcelService {
	String jdbcURL = "jdbc:mysql://127.0.0.1:3306/proyectofinal?serverTimezone=UTC#";
	String username = "root";
	String password = "1234";

	@Autowired
	@Qualifier("momentumJpaRepository")
	MomentumJpaRepository momentumJpaRepository;

	@Override
	@PostConstruct
	public Boolean readFile() {
		String excelFilePath = "C:/apache-tomcat-8.5.42/temp/Registros Momentum.xls";
		Connection connection = null;
		try {
			long start = System.currentTimeMillis();
			FileInputStream inputStream = new FileInputStream(excelFilePath);
			Workbook workbook = new HSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = firstSheet.iterator();
			connection = DriverManager.getConnection(jdbcURL, username, password);
			connection.setAutoCommit(false);
			String sql = "INSERT INTO rawmomentum (name, date) VALUES (?, ?)";
			PreparedStatement statement = connection.prepareStatement(sql);
			rowIterator.next(); 
			
			while (rowIterator.hasNext()) {
				Row nextRow = rowIterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				
				while (cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
					
					switch (columnIndex) {
					case 2:
						String name = nextCell.getStringCellValue();
						statement.setString(1, name);
						break;
					case 4:
						String date = nextCell.getStringCellValue();
						statement.setString(2, date);
						break;
					default:
					}

				}
				statement.addBatch();
			}
			statement.executeBatch();
			connection.commit();

			
			System.out.println("-----------------------------------------------------------");
			System.out.println("| Loading Data. Please wait. This can take a few minutes. |");
			System.out.println("| Loading Data. Please wait. This can take a few minutes. |");
			System.out.println("| Loading Data. Please wait. This can take a few minutes. |");
			System.out.println("-----------------------------------------------------------");

			sql = "SELECT * from rawmomentum";
			statement = connection.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			
			if(momentumJpaRepository.count() != 0) {
				System.out.println("|		Data already exists in table momentums.           |");
				System.out.println("|		Please wait. This can take a little longer.       |");
				System.out.println("-----------------------------------------------------------");
			}
			
			while (rs.next()) {
				String name = rs.getString("name");
				String time = rs.getString("date").substring(11, 19);
				String day = rs.getString("date").substring(8, 10);
				String month = rs.getString("date").substring(5, 7);
				String year = rs.getString("date").substring(0, 4);

				Momentum momentum = new Momentum(name, day, month, year, time);
				List<Momentum> mExist=momentumJpaRepository.findByNameAndDayAndMonthAndYearAndTime(name, day, month, year, time);
				
				if(mExist.size()==0) { 
					momentumJpaRepository.save(momentum);
				}

				
			}
			connection.close();
			long end = System.currentTimeMillis();
			System.out.printf("Import done in %d ms\n", (end - start));
			return true;
		} catch (IOException ex1) {
			System.out.println("Error reading file");
			ex1.printStackTrace();
			return false;
		} catch (SQLException ex2) {
			System.out.println("Database error");
			ex2.printStackTrace();
			return false;
		}

	}

}
