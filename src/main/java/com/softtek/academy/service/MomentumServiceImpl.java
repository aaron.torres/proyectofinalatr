package com.softtek.academy.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.softtek.academy.entities.Momentum;
import com.softtek.academy.repositories.MomentumJpaRepository;

@Service("momentumServiceImpl")
public class MomentumServiceImpl implements MomentumService {

	@Autowired
	@Qualifier("momentumJpaRepository")
	MomentumJpaRepository momentumJpaRepository;

	@Autowired
	@Qualifier("excelServiceImpl")
	ExcelServiceImpl excelServiceImpl;

	@Override
	public Double obtainMonthHours(String name, String month) {
		Double monthHours = 0.0;
		List<Momentum> momenList = new ArrayList<Momentum>();
		momenList = momentumJpaRepository.findByNameAndMonth(name, month); 
		System.out.println("Elementos en la lista:" + momenList.size());
		int i = 0;
		int j = 1;
		while (i != (momenList.size() - 1)) {
			Momentum m1 = momenList.get(i);
			Momentum m2 = momenList.get(j);
			if (m1.getDay().equals(m2.getDay()) && m1.getMonth().equals(m2.getMonth())
					&& m1.getYear().equals(m2.getYear())) {
				Double h1 = Double.parseDouble(m1.getTime().substring(0, 2));
				Double h2 = Double.parseDouble(m2.getTime().substring(0, 2));
				Double res = h2 - h1;
				monthHours = monthHours + res;
			}
			i++;
			j++;
		}
		return monthHours;
	}

	@Override
	public List<Momentum> getMomentumbyDate(String name, String day, String month, String year, String endday,
			String endmonth, String endyear) {
		int day1, month1, year1;
		int day2, month2, year2;
		day1 = Integer.parseInt(day);
		day2 = Integer.parseInt(endday);
		month1 = Integer.parseInt(month);
		month2 = Integer.parseInt(endmonth);
		year1 = Integer.parseInt(year);
		year2 = Integer.parseInt(endyear);
		List<Momentum> filteredList = new ArrayList<Momentum>();
		List<Momentum> m = momentumJpaRepository.findByName(name);
		System.out.println("Cantidad de registros originales:" + m.size());
		int i = 0;
		while (i != m.size()) {
			Momentum momentum = m.get(i);
			if (Integer.parseInt(momentum.getDay()) >= day1 && Integer.parseInt(momentum.getDay()) <= day2) {
				if (Integer.parseInt(momentum.getYear()) >= year1 && Integer.parseInt(momentum.getYear()) <= year2) {
					if (Integer.parseInt(momentum.getMonth()) >= month1
							&& Integer.parseInt(momentum.getMonth()) <= month2) {
						filteredList.add(momentum);
					}
				}
			}
			i++;
		}
		System.out.println("Cantidad de registros filtrados:" + filteredList.size());
		return filteredList;
	}

	@Override
	public List<Momentum> getMomentumByMonth(String month) { 
		List<Momentum> m = momentumJpaRepository.findByMonth(month);
		System.out.println("Cantidad de elementos:" + m.size());
		return m;
	}

	@Override
	public List<Momentum> getMomentumsbyDate(String day, String month, String year, String endday, String endmonth,
			String endyear) {
		int day1, month1, year1;
		int day2, month2, year2;
		day1 = Integer.parseInt(day);
		day2 = Integer.parseInt(endday);
		month1 = Integer.parseInt(month);
		month2 = Integer.parseInt(endmonth);
		year1 = Integer.parseInt(year);
		year2 = Integer.parseInt(endyear);
		List<Momentum> filteredList = new ArrayList<Momentum>();
		List<Momentum> m = momentumJpaRepository.findAll();
		System.out.println("Cantidad de registros originales:" + m.size());
		int i = 0;
		while (i != m.size()) {
			Momentum momentum = m.get(i);
			if (Integer.parseInt(momentum.getDay()) >= day1 && Integer.parseInt(momentum.getDay()) <= day2) {
				if (Integer.parseInt(momentum.getYear()) >= year1 && Integer.parseInt(momentum.getYear()) <= year2) {
					if (Integer.parseInt(momentum.getMonth()) >= month1
							&& Integer.parseInt(momentum.getMonth()) <= month2) {
						filteredList.add(momentum);
					}
				}
			}
			i++;
		}
		System.out.println("Cantidad de registros filtrados:" + filteredList.size());
		return filteredList;
	}

	@Override //Se utiliza para testing
	public List<Momentum> listAllMomentums() {
		return momentumJpaRepository.findAll();
	}
}
