package com.softtek.academy.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.softtek.academy.entities.Momentum;

@Repository("momentumJpaRepository")
public interface MomentumJpaRepository extends JpaRepository<Momentum, Serializable> {

	public abstract Momentum findById(int id);

	public abstract List<Momentum> findByName(String name);

	public abstract List<Momentum> findByNameAndMonth(String name, String month);

	public abstract List<Momentum> findByMonth(String month);

	public abstract Momentum findByIdAndName(int id, String name);

	public abstract List<Momentum> findByNameAndDayAndMonthAndYear(String name, String day, String month, String year);

	public abstract List<Momentum> findByDayAndMonthAndYear(String day, String month, String year);

	public abstract List<Momentum> findByNameAndDayAndMonthAndYearAndTime(String name, String day, String month,
			String year, String time);
}
