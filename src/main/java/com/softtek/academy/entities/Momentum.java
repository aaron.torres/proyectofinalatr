package com.softtek.academy.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "momentums")
public class Momentum {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "day")
	private String day;
	@Column(name = "month")
	private String month;
	@Column(name = "year")
	private String year;
	@Column(name = "time")
	private String time;
	@Transient
	private double HorasTotales;

	public Momentum(String name, String day, String month, String year, String time) {
		super();
		this.name = name;
		this.day = day;
		this.month = month;
		this.year = year;
		this.time = time;
	}
	
	public Momentum(double HorasTotales, String name, String month) {
		super();
		this.HorasTotales = HorasTotales;
		this.name = name;
		this.month = month;
	
	}
	
	

	public double getHorasTotales() {
		return HorasTotales;
	}



	public void setHorasTotales(double horasTotales) {
		HorasTotales = horasTotales;
	}



	@Override
	public String toString() {
		return "Momentum [id=" + id + ", name=" + name + ", day=" + day + ", month=" + month + ", year=" + year
				+ ", time=" + time + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Momentum() {
	};

}
