package com.softtek.academy.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.softtek.academy.entities.Momentum;
import com.softtek.academy.service.ExcelService;
import com.softtek.academy.service.MomentumService;

@Controller("viewsController")
@RequestMapping("/view/v2")
public class ViewsController {

	@Autowired
	@Qualifier("momentumServiceImpl")
	private MomentumService momentumService;


//--// PUNTO #1----------------------------------------------------------------------
	
	@GetMapping("/user")
	public ModelAndView puntounoform() {
		return new ModelAndView("puntouno");
	}
	
	@GetMapping("/userget") 
	public String horastotales(Model model, String name, String month) {
		Double HorasTotales;
		HorasTotales=momentumService.obtainMonthHours(name, month);
		model.addAttribute("HorasTotales", HorasTotales);
		return "puntounov";
	}

//--// PUNTO #2 ----------------------------------------------------------------------	
	
	@GetMapping("/users")
	public ModelAndView puntodosform() {
		return new ModelAndView("puntodos");
	}

	
	@PostMapping("/usersres")
	public ModelAndView getBetweenDates(Model model,
			@RequestParam("name")String name,
			@RequestParam("day")String day,@RequestParam("month")String month,@RequestParam("year")String year,
			@RequestParam("endday")String endday,@RequestParam("endmonth")String endmonth,@RequestParam("endyear")String endyear) {
		ModelAndView mav=new ModelAndView("showMomentums");
		mav.addObject("momentums",
				momentumService.getMomentumbyDate(name, day, month, year, endday, endmonth, endyear));
		return mav;
	}


//--// PUNTO #3 ----------------------------------------------------------------------
	@GetMapping("/period")
	public ModelAndView puntotresform() {
		return new ModelAndView("puntotres");
	}
	
	@GetMapping("/periodget") 
	public ModelAndView checkbyMonth(Model model, String month) {
		List<Momentum> momList=momentumService.getMomentumByMonth(month);
		ModelAndView mav=new ModelAndView("showMomentums");
		mav.addObject("momentums",momList);
		return mav;
	}

//--// PUNTO #4----------------------------------------------------------------------
	
	@GetMapping("/periods")
	public ModelAndView puntocuatroform() {
		return new ModelAndView("puntocuatro");
	}

	@PostMapping("/periodres")
	public ModelAndView getBetweenDates(Model model,
			@RequestParam("day")String day,@RequestParam("month")String month,@RequestParam("year")String year,
			@RequestParam("endday")String endday,@RequestParam("endmonth")String endmonth,@RequestParam("endyear")String endyear) {
		ModelAndView mav=new ModelAndView("showMomentums");
		mav.addObject("momentums",
				momentumService.getMomentumsbyDate(day, month, year, endday, endmonth, endyear));
		return mav;
	}


}