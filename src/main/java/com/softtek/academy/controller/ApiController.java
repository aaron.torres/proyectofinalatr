package com.softtek.academy.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.softtek.academy.entities.Momentum;
import com.softtek.academy.service.ExcelService;
import com.softtek.academy.service.MomentumService;

@RestController("apiController")
@RequestMapping("/api/v1")
public class ApiController {

	@Autowired
	@Qualifier("momentumServiceImpl")
	private MomentumService momentumService;


//--// PUNTO #1----------------------------------------------------------------------
	@GetMapping("/users/{is}/{mes}") 
    public List<Momentum> readFile(Model model, @PathVariable("is") String name, @PathVariable("mes") String month) {
        Double HorasTotales;
        List<Momentum> m=new ArrayList<Momentum>();
        HorasTotales = momentumService.obtainMonthHours(name, month);
            Momentum hours=new Momentum(HorasTotales, name, month);
            m.add(hours);
        return m;
    }

//--// PUNTO #2 ----------------------------------------------------------------------	
	

	
	@PostMapping("/usersres") 
	public List<Momentum> getBetweenDates(Model model, @RequestParam("name") String name,
			@RequestParam("day") String day, @RequestParam("month") String month, @RequestParam("year") String year,
			@RequestParam("endday") String endday, @RequestParam("endmonth") String endmonth,
			@RequestParam("endyear") String endyear) {
		List<Momentum> mom = momentumService.getMomentumbyDate(name, day, month, year, endday, endmonth, endyear);
		return mom;
	}

//--// PUNTO #3 ----------------------------------------------------------------------
	
	@GetMapping("/period/{mes}") 
	public List<Momentum> checkbyMonth(Model model, @PathVariable("mes") String month) {
		List<Momentum> momList = momentumService.getMomentumByMonth(month);
		return momList;
	}

//--// PUNTO #4----------------------------------------------------------------------
	

	@PostMapping("/periodres")
	public List<Momentum> getBetweenDates(Model model, @RequestParam("day") String day,
			@RequestParam("month") String month, @RequestParam("year") String year,
			@RequestParam("endday") String endday, @RequestParam("endmonth") String endmonth,
			@RequestParam("endyear") String endyear) {
		return momentumService.getMomentumsbyDate(day, month, year, endday, endmonth, endyear);
	}

}
