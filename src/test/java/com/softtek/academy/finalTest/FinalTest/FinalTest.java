package com.softtek.academy.finalTest.FinalTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import com.softtek.academy.entities.Momentum;
import com.softtek.academy.repositories.MomentumJpaRepository;
import com.softtek.academy.service.ExcelServiceImpl;
import com.softtek.academy.service.MomentumService;

@SpringBootTest
class FinalTest {

	@Autowired
	@Qualifier("momentumServiceImpl")
	private MomentumService momentumService;

	@Autowired
	@Qualifier("momentumJpaRepository")
	MomentumJpaRepository momentumJpaRepository;
	@Autowired
	@Qualifier("excelServiceImpl")
	ExcelServiceImpl excelServiceImpl;

	@Test
	void contextLoads() {
	}
	
	//Persist Object Test
		@Test
		public void repositoryTest() {
			String name = "ATOR";
			String day = "11";
			String month = "12";
			String year = "2019";
			String time = "08:30:01";
			Momentum expected = new Momentum(name, day, month, year, time);
			Momentum actual = momentumJpaRepository.save(expected); 
			assertEquals(expected, actual);
		}

	@Test
	public void serviceTest() {
		String month1 = "02";
		String month2 = "03";
		List<Momentum> unexpectedList = momentumJpaRepository.findByMonth(month1);
		List<Momentum> actualList = new ArrayList<Momentum>();
		actualList = momentumService.getMomentumByMonth(month2);
		assertNotEquals(unexpectedList, actualList);
	}

	@Test
	public void readFileTest() {
		Boolean expected = true;
		Boolean actual = excelServiceImpl.readFile();
		assertEquals(expected, actual);
	}

	@Test
	public void momentumServiceTest() {
		List<Momentum> expectedList = momentumJpaRepository.findAll();
		List<Momentum> actualList = momentumService.listAllMomentums();
		assertEquals(expectedList.size(), actualList.size());
	}

}
